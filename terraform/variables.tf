variable "container_cpu" {
  description = "The number of cpu units used by the task"
  default     = 1000
}

variable "container_memory" {
  description = "The amount (in MiB) of memory used by the task"
  default     = 2048
}

variable "name" {
  default = "echo-project"
  type = string
}

variable "environment" {
  default = "dev"
  type = string
}

variable "container_image"{

}

variable "image_tag"{
  default = "0.0.1-SNAPSHOT"
}

variable "container_port" {
    default = 8080
}

variable "alb_security_group" {
  
}

variable "public_subnets" {
  type = list
}

variable "vpc_id" {
  
}

variable "ecs_arn" {
  
}

variable "ecs_security_group" {
  
}

variable "private_subnets" {
  type = list
}