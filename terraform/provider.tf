terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = "3.63.0"
    }
  }

  backend "s3" {
    bucket = "echo-project-state"
    key    = "echo-project.tfstate"
    region = "ap-southeast-1"
    dynamodb_table = "echo-project-lock"
  }
}

provider "aws"{
    region = "ap-southeast-1"
}