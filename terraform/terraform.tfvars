alb_security_group = [
  "sg-0e4e2cc43ede34470",
]
container_image = "084767242532.dkr.ecr.ap-southeast-1.amazonaws.com/echo-project"
ecs_arn = "arn:aws:ecs:ap-southeast-1:084767242532:cluster/echo-project-cluster"
ecs_security_group = [
  "sg-01b1106dab2462299",
]
private_subnets = [
  "subnet-06aedeaf27f373a31",
  "subnet-0df986c3ea6a09faf",
]
public_subnets = [
  "subnet-09464e2b4335d4029",
  "subnet-0a1b3089786317534",
]
vpc_id = "vpc-05d14f36bee0e1837"