package com.springboot.dto.request;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class PersonRequest {
	
	public String name;
	public String mobileNumber;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
