package com.springboot.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.dto.request.PersonRequest;
import com.springboot.dto.response.PersonResponse;

@RestController
@RequestMapping("/api/v1")
public class EchoController {
	
	@PostMapping(
			value = "/echo",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getecho(@RequestBody PersonRequest person,
			@RequestHeader( value = "User-Agent", required=false) String userAgent, HttpServletRequest request){
		String remoteAddr = "";
		PersonResponse personResponse = new PersonResponse();
		personResponse.setName(person.getName());
		personResponse.setMobileNumber(person.getMobileNumber());
		if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
		personResponse.setIp(remoteAddr);
		personResponse.setUserAgent(userAgent);
		
		
		return new ResponseEntity<>(personResponse, HttpStatus.OK);
	}

	@ResponseBody
	@GetMapping(value = "health")
    ResponseEntity  health() {
		return ResponseEntity.ok().body("OK");
    }


}
