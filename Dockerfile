FROM openjdk:11.0.13-slim-buster
RUN mkdir /app
COPY target/*.jar /app/student.jar
WORKDIR /app
CMD ["java", "-jar","student.jar"]